# Descripción del proyecto

Este es nuestro primer proyecto usando la herramienta de versionamiento de código git y en interacción con la plataforma Gitlab.

## Tecnologías

  - Lenguaje C.

## Instrucciones para su instalación y uso

### Cómo compilarlo

```
gcc hello-world.c
```

### Uso

Luego de compilado, obtendremos un archivo ejecutable con el nombre de `a.out`.

# TODOs

  - Ampliar funciones del código para imprimir otros mensajes.
  - Uso de CI (Integración Continua)
  - Probar los `merge request`
  - Agregar más usuarios y subir código concurrentemente y lidiar con eso..
  - Trabajar con las ramas y discutir sobre una metodología básica para ello.
  - Manejo de los `remote`, `push`, etc.


